ad_library {

    Initialization for auth-ldap-adldapsearch   
    @author Frank Bergmann (frank.bergmann@project-open.com)
    @creation-date 10 April, 2017
    @cvs-id $Id: intranet-helpdesk-init.tcl,v 1.5 2014/11/02 20:20:54 cvs Exp $
}

# Initialize the import "semaphore" to 0.
# There should be only one thread importing at a time...
nsv_set auth_ldap_adldapsearch_sweeper sweeper_p 0

# Check for changed files every X minutes
ad_schedule_proc -thread t [parameter::get_from_package_key -package_key intranet-cost -parameter SourceForgeTrackerSweeperInterval -default [expr 3600 * 24]] auth_ldap_adldapsearch_import_sweeper


# ToDo: Sweeper doesn't exist yet

