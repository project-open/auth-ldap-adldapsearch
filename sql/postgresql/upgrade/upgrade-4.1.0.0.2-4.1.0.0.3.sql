-- upgrade-4.1.0.0.2-4.1.0.0.3.sql
SELECT acs_log__debug('/packages/auth-ldap-adldapsearch/sql/postgresql/upgrade/upgrade-4.1.0.0.2-4.1.0.0.3.sql','');


create or replace function inline_0 () 
returns integer as $body$
DECLARE
	v_count			integer;
	v_object_id		integer;
BEGIN
	select	count(*) into v_count
	from	acs_sc_impls
	where	impl_name = 'LDAP' and
		impl_contract_name = 'auth_authentication' and
		impl_owner_name = 'auth-ldap-adldapsearch';

	IF v_count > 0 THEN return 1; END IF;

	PERFORM acs_sc_impl__new('auth_authentication', 'LDAP', 'auth-ldap-adldapsearch');
	PERFORM acs_sc_impl__new('auth_password', 'LDAP', 'auth-ldap-adldapsearch');
	PERFORM acs_sc_impl__new('auth_registration', 'LDAP', 'auth-ldap-adldapsearch');
	PERFORM acs_sc_impl__new('auth_user_info', 'LDAP', 'auth-ldap-adldapsearch');

	return 0;
END;$body$ language 'plpgsql';
SELECT inline_0 ();
DROP FUNCTION inline_0 ();

