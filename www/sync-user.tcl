# /auth-ldap-adldapsearch/www/sync-user

ad_page_contract {
    Sync a single user with LDAP according to parameters
    @author frank.bergmann@project-open.com } {
	{ user_id:integer "" }
    }

set current_user_id [auth::require_login] 
set current_user_is_admin_p [im_is_user_site_wide_or_intranet_admin $current_user_id] 
if {!$current_user_is_admin_p} {
    ad_return_complaint 1 "<li>[_ intranet-core.lt_You_need_to_be_a_syst]"
    return
}

if {"" eq $user_id} { set user_id $current_user_id }


# ---------------------------------------------------------------
# Get the parameters
# ---------------------------------------------------------------

db_1row auth_info "
	select	a.authority_id,
		a.auth_impl_id as impl_id,
		u.username
	from	users u,
		auth_authorities a
	where	u.user_id = :user_id and
		a.authority_id = u.authority_id
"

set parameters [auth::driver::get_parameter_values -authority_id $authority_id -impl_id $impl_id] 
array set result_hash [auth::ldap::authentication::Sync $username $parameters $authority_id]

set html ""
foreach key [array names result_hash] {
    set val $result_hash($key)
    append html "$key\t\t = $val\n"
}

ad_return_complaint 1 "<pre>$html</pre>"
