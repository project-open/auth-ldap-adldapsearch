# /auth-ldap-adldapsearch/www/import-users

ad_page_contract {
    Import all users from LDAP according to parameters
    @author frank.bergmann@project-open.com } {
	{authority_id:integer ""}
    }

set current_user_id [auth::require_login] 
set current_user_is_admin_p [im_is_user_site_wide_or_intranet_admin $current_user_id]
if {!$current_user_is_admin_p} {
    ad_return_complaint 1 "<li>[_ intranet-core.lt_You_need_to_be_a_syst]"
    return
}

# ---------------------------------------------------------------
# Select authority_id if not specified
# ---------------------------------------------------------------

if {"" eq $authority_id} {
    set auths {}
    db_foreach auth "select * from auth_authorities order by sort_order" {
        set url [export_vars -base "/auth-ldap-adldapsearch/import-users" {authority_id}]
	lappend auths "<tr><td><a href=$url>$pretty_name</a></td></tr>\n"
    }
    doc_return 200 "text/html" "
        [im_header]
        <table><tr><td>Authority</td></tr><tr>
        [join $auths ""]
        </tr></table>
        [im_footer]
    "
    ad_script_abort
}

# ---------------------------------------------------------------
# Get the parameters
# ---------------------------------------------------------------

db_1row auth_info "
	select	a.authority_id,
		a.auth_impl_id as impl_id
	from	auth_authorities a
		wherea.authority_id = :authority_id
"

set parameters [auth::driver::get_parameter_values -authority_id $authority_id -impl_id $impl_id]
array set result_hash [auth::ldap::batch_import::import_users $parameters $authority_id]

set html ""
foreach key [array names result_hash] {
    set val $result_hash($key)
    append html "$key\t\t = $val\n"
}

ad_return_complaint 1 "<pre>$html</pre>"
